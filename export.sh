#!/usr/bin/env bash

if test "$1" != ""; then
	cd "$1" || exit;
fi

if ! test -e "storage"; then
	echo "Unable to find storage directory. Please make sure to pass the path to the zotero files as first argument."
	exit 1
fi

EXPORT_DIR="zotero_pdf_export"
DB=zotero.sqlite
SQL=sqlite3
QUERY="$SQL $DB"

ROWS=$($QUERY 'select c.collectionName, c.dirname, i.key from items i
join itemAttachments ia on ia.itemID = i.itemID 
join collectionItems ci on ci.itemID = i.itemID or ci.itemID = ia.parentItemID
join (
with recursive
	recItems(colId, colName, dirname) AS (
		SELECT collectionID, collectionName, collectionName as dirname FROM collections WHERE parentCollectionID IS NULL
		UNION 
		SELECT collectionID, collectionName, recItems.dirname || "/" || collectionName as dirname FROM collections, recItems WHERE collections.parentCollectionID = recItems.colId
	)
SELECT colId as collectionID, colName as collectionName, dirname FROM recItems
) as c on c.collectionID = ci.collectionID
where ia.contentType = "application/pdf"')

rm -rf $EXPORT_DIR
while IFS="|" read COLLECTION DIR_PATH KEY; do
	mkdir -p "$EXPORT_DIR/$DIR_PATH"
	cp ./storage/$KEY/*.pdf "$EXPORT_DIR/$DIR_PATH"
done <<< "$ROWS"